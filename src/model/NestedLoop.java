package model;

public class NestedLoop {
	
	public String Nested1(int count){
		String x = "";
		
		for(int i = 1; i<=count-1;i++){
			for(int j=1;j<=count;j++){			
				x=x+"*";
			}
			x=x+"\n";
		}
		return x;
}
	public String Nested2(int count){
		String x = "";
		
		for(int i = 1; i<=count;i++){
			for(int j=1;j<=count-1;j++){			
				x=x+"*";
			}
			x=x+"\n";
		}
		return x;
	}
	
	public String Nested3(int count){
		String x = "";
		
		for(int i = 1; i<=count;i++){
			for(int j=1;j<=i;j++){			
				x=x+"*";
			}
			x=x+"\n";
		}
		return x;
	}
	
	public String Nested4(int count){
		String x = "";
		
		for(int i = 1; i<=count-1;i++){
			for(int j=1;j<=count+1;j++){
				if(j%2==0){
					x=x+"*";
				}
				else{
					x=x+"-";
				}
			}
			x=x+"\n";
		}
		return x;
	}
	
	public String Nested5(int count){
		String x = "";
		
		for(int i = 1; i<=count-1;i++){
			for(int j=1;j<=count+1;j++){
				if((i+j)%2==0){
					x=x+"*";
				}
				else{
					x=x+" ";
				}
			}
			x=x+"\n";
		}
		return x;
	} 
}
