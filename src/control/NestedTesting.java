package control;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import view.GUI;

public class NestedTesting {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}

	}
	
	GUI frame;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new NestedTesting();
	}

	public NestedTesting() {
		frame = new GUI();
		list = new ListenerMgr();
		frame.setListener(list);
		
		
	}


	ActionListener list;
}


/*
 
NestedLoop test = new NestedLoop();
	System.out.println(test.Nested1(4));
	System.out.println(test.Nested2(4));
	System.out.println(test.Nested3(4));
	System.out.println(test.Nested4(4));
	System.out.println(test.Nested5(4));

*/