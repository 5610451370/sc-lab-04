package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import model.NestedLoop;


public class GUI extends JFrame{
	NestedLoop nestedloop = new NestedLoop();

	
	private JTextArea showtext;
	private JButton endbutton;
	private String str;

	public GUI() {
		createFrame();
	}

	public void createFrame() {
		JFrame frame = new JFrame("Nested Loop");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 400);
		frame.setLayout(null);
		frame.setResizable(false);
		
		JPanel combo = new JPanel();
		combo.setBounds(0, 0, 500, 50);
		String[] nestedStrings = { "NestedLoop1", "NestedLoop2", "NestedLoop3", "NestedLoop4", "NestedLoop5"};
		JComboBox<String> nestedList = new JComboBox<String>(nestedStrings);
		combo.add(nestedList);
		
	    
		JTextField inputtext = new JTextField();
		inputtext.setBounds(30, 64, 400, 28);
		
		JLabel text = new JLabel("<<<  Enter input Here");
		text.setBounds(440, 61, 160, 30);
		
		JTextArea showtext = new JTextArea();
		showtext.setBounds(30, 100, 400, 250);
		showtext.setBackground(Color.WHITE);
		
		JButton run = new JButton("RUN");
		run.setBounds(465, 100, 100, 40);
		run.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String show = inputtext.getText();
				int s = Integer.parseInt(show);
				if(nestedList.getSelectedIndex()==0){
					showtext.setText(nestedloop.Nested1(s));
				}
				if(nestedList.getSelectedIndex()==1){
					showtext.setText(nestedloop.Nested2(s));
				}
				if(nestedList.getSelectedIndex()==2){
					showtext.setText(nestedloop.Nested3(s));
				}
				if(nestedList.getSelectedIndex()==3){
					showtext.setText(nestedloop.Nested4(s));
				}
				if(nestedList.getSelectedIndex()==4){
					showtext.setText(nestedloop.Nested5(s));
				}
				
				}
		});
		
		
		this.endbutton = new JButton("Close");
		endbutton.setBounds(465, 310, 100, 40);
		
		frame.add(showtext);
		frame.add(run);
		frame.add(endbutton);
		frame.add(combo);
		frame.add(inputtext);
		frame.add(text);
		frame.setVisible(true);
	}




	public void setListener(ActionListener list) {
		endbutton.addActionListener(list);
	}

}
